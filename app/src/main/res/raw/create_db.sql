CREATE TABLE languages(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    language TEXT NOT NULL
);

CREATE TABLE localised_strings (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    language_fk INT NOT NULL,
    FOREIGN KEY (language_fk) REFERENCES languages(id)
);

CREATE TABLE dictionary_types(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    type TEXT NOT NULL
);

CREATE TABLE dictionaries(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    type_fk INT NOT NULL,
    name_fk INT,
    FOREIGN KEY (type_fk) REFERENCES dictionary_types(id),
    FOREIGN KEY (name_fk) REFERENCES localised_strings(id)
);

CREATE TABLE domains(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name_fk INT NOT NULL,
    FOREIGN KEY (name_fk) REFERENCES localised_strings(id)
);

CREATE TABLE lexicons(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    version REAL NOT NULL
);

CREATE TABLE node_positions(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    position TEXT NOT NULL UNIQUE
);

CREATE TABLE relations_types(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name_fk INT NOT NULL,
    short_text_fk INT NOT NULL,
    display_text_fk INT NOT NULL,
    parent_fk INT,
    node_position_fk INT NOT NULL,
    FOREIGN KEY (name_fk) REFERENCES localised_strings(id),
    FOREIGN KEY (short_text_fk) REFERENCES localised_strings(id),
    FOREIGN KEY (display_text_fk) REFERENCES localised_strings(id),
    FOREIGN KEY (parent_fk) REFERENCES relations_types(id),
    FOREIGN KEY (node_position_fk) REFERENCES node_positions(rowid)
);

CREATE TABLE synsets(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    lexicon_fk INT NOT NULL,
    split INT DEFAULT 0,
    definition TEXT
);

CREATE TABLE synset_examples(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    synset_fk INT NOT NULL,
    example TEXT NOT NULL,
    type TEXT,
    FOREIGN KEY (synset_fk) REFERENCES synsets(id)
);

CREATE TABLE synset_relations(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    source_fk INT NOT NULL,
    target_fk INT NOT NULL,
    relation_fk INT NOT NULL,
    FOREIGN KEY (source_fk) REFERENCES synset(id),
    FOREIGN KEY (target_fk) REFERENCES synset(id),
    FOREIGN KEY (relation_fk) REFERENCES relations_types(id),
    CONSTRAINT synset_relation_unique UNIQUE (source_fk, target_fk, relation_fk)
);

CREATE TABLE parts_of_speech(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name_fk INT NOT NULL UNIQUE,
    FOREIGN KEY (name_fk) REFERENCES localised_strings(id)
);

CREATE TABLE senses(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    word_fk INT NOT NULL,
    variant INT NOT NULL DEFAULT 0,
    part_of_speech_fk INT NOT NULL,
    domain_fk INT NOT NULL,
    lexicon_fk INT NOT NULL,
    synset_fk INT,
    synset_position INT DEFAULT 0,
    definition TEXT,
    link TEXT,
    FOREIGN KEY (word_fk) REFERENCES words(id),
    FOREIGN KEY (part_of_speech_fk) REFERENCES parts_of_speech(id),
    FOREIGN KEY (domain_fk) REFERENCES domains(id),
    FOREIGN KEY (lexicon_fk) REFERENCES lexicons(id),
    FOREIGN KEY (synset_fk) REFERENCES synsets(id)
);

CREATE TABLE sense_examples(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sense_fk INT NOT NULL,
    example TEXT NOT NULL,
    type TEXT,
    FOREIGN KEY (sense_fk) REFERENCES senses(id)
);

CREATE TABLE sense_relations(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    source_fk INT NOT NULL,
    target_fk INT NOT NULL,
    relation_fk INT NOT NULL,
    FOREIGN KEY (source_fk) REFERENCES senses(id),
    FOREIGN KEY (target_fk) REFERENCES senses(id),
    FOREIGN KEY (relation_fk) REFERENCES relations_types(id),
    CONSTRAINT sense_relation_unique UNIQUE (source_fk, target_fk, relation_fk)
);

CREATE TABLE words(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    word TEXT NOT NULL UNIQUE
);

CREATE TABLE emotional_annotations(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sense_fk INT NOT NULL,
    markedness_fk INT NOT NULL,
    FOREIGN KEY (sense_fk) REFERENCES senses(id),
    FOREIGN KEY (markedness_fk) REFERENCES dictionaries(id)
);

CREATE TABLE sense_emotions(
    annotation_fk INT NOT NULL,
    emotion_fk INT NOT NULL,
    FOREIGN KEY (annotation_fk) REFERENCES emotional_annotations(id),
    FOREIGN KEY(emotion_fk) REFERENCES dictionaries(id),
    PRIMARY KEY (annotation_fk, emotion_fk)
);

CREATE TABLE sense_valuations(
    annotation_fk INT NOT NULL,
    valuation_fk INT NOT NULL,
    FOREIGN KEY (annotation_fk) REFERENCES emotional_annotations(id),
    FOREIGN KEY(valuation_fk) REFERENCES dictionaries(id),
    PRIMARY KEY (annotation_fk, valuation_fk)
);

CREATE INDEX word_index ON words (word);

