package pl.edu.pwr.plwordnet.errors

import android.net.Uri
import java.lang.Exception

class UnknownUriException(uri:Uri): Exception("Unknown URI$uri")