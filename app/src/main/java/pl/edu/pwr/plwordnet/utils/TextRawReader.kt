package pl.edu.pwr.plwordnet.utils

import android.content.Context
import pl.edu.pwr.plwordnet.R
import java.util.*

class TextRawReader{

    companion object {
        fun read(resoruceId : Int, context: Context): String{
            val stream = context.resources.openRawResource(resoruceId)
            val reader = Scanner(stream)
            val stringBuilder = StringBuilder()
            var line :String?
            while(reader.hasNext()){
                line = reader.nextLine()
                stringBuilder.append(line)
            }
            return stringBuilder.toString()
        }
    }

}