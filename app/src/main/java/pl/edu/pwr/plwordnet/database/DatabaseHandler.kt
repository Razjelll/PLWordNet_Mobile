package pl.edu.pwr.plwordnet.database

import android.content.ContentResolver
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import pl.edu.pwr.plwordnet.R
import pl.edu.pwr.plwordnet.utils.TextRawReader

class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DBName, null, DBVersion) {

    companion object {
        val Tag = "DatabaseHandler"
        val DBName = "create_db"
        val DBVersion = 1
    }

    var mContext : Context?= context
//    var mDb : SQLiteDatabase
    private val mContentResolver : ContentResolver

    init {
//        mDb = this.writableDatabase
        mContentResolver = context.contentResolver
    }

    override fun onCreate(database : SQLiteDatabase){
        val query = TextRawReader.read(R.raw.create_db, mContext!!)
//        database.execSQL(query)
        val queries = query.split(";")
        queries.forEach {
            System.out.println(it)
            database.execSQL(it) }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}