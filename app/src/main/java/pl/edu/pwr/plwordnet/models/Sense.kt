package pl.edu.pwr.plwordnet.models

class Sense {
    private var id : Long
        get() = this.id
        set(value) {id = value}

    // TODO wstawić Word

    private var variant : Int
        get() = this.variant
        set(value) {variant  = value}

    // TODO wstawić część mowy, domenę, leksykon, synset

    private var synsetPosition : Int
        get() = this.synsetPosition
        set(value) {synsetPosition = value}

}