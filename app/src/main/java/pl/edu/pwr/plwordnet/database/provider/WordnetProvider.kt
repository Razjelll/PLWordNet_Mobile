package pl.edu.pwr.plwordnet.database.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.support.v4.util.SparseArrayCompat
import android.text.TextUtils
import pl.edu.pwr.plwordnet.database.DatabaseHandler
import pl.edu.pwr.plwordnet.errors.UnknownUriException
import java.lang.IllegalArgumentException

class WordnetProvider : ContentProvider() {

    private val ALL = 1;
    private val SINGLE = 2;

    private val SENSES_ALL = 1;
    private val SENSES_SINGLE = 2;
    private val WORD_ALL = 3;
    private val WORD_SINGLE = 4;

    private val STANDARD_ID = "id"

    companion object {
        val AUHORITY = "pl.edu.pwr.plwordnet.database.provider.WordnetProvider"
        private val SENSES_TABLE = "senses"
        private val WORDS_TABLE = "words"
        val SENSES_URI :Uri = Uri.parse("content://$AUHORITY/$SENSES_TABLE")
        val WORDS_URI :Uri = Uri.parse("content://$AUHORITY/$WORDS_TABLE")

    }

    private val mUriMatcher = UriMatcher(UriMatcher.NO_MATCH)
    private var mDb : DatabaseHandler? = null

    init {
        mUriMatcher.addURI(AUHORITY, SENSES_TABLE, SENSES_ALL)
        mUriMatcher.addURI(AUHORITY, "$SENSES_TABLE/#", SENSES_SINGLE)
        mUriMatcher.addURI(AUHORITY, WORDS_TABLE, WORD_ALL)
        mUriMatcher.addURI(AUHORITY, "$WORDS_TABLE/#", WORD_SINGLE)
        // TODO wstawić resztę
    }

    override fun query(uri: Uri, projection: Array<String>?, selection: String?, selectionArgs: Array<String>?, sortOrder: String?): Cursor? {
        val tableName = getTableName(uri)
        val queryBuilder = SQLiteQueryBuilder()
        queryBuilder.tables = tableName
        val mode = getMode(uri)
        // TODO wstawienie whera
        when (mode){
            ALL -> {}
            SINGLE -> serveSingleQuery(queryBuilder, uri)
        }
        val cursor = queryBuilder.query(mDb?.readableDatabase, projection, selection, selectionArgs, null, null, sortOrder)
        cursor.setNotificationUri(context.contentResolver, uri)
        return cursor
    }

    private fun serveSingleQuery(queryBuilder: SQLiteQueryBuilder, uri:Uri) {
        queryBuilder.appendWhere("$STANDARD_ID="+uri.lastPathSegment)
    }

    override fun onCreate(): Boolean {
        mDb = DatabaseHandler(context)
        return false
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        val uriType = mUriMatcher.match(uri)
        val tableName = getTableName(uriType)
        val rowsUpdated:Int
        when(uriType){
            SENSES_ALL -> rowsUpdated = updateSingle(tableName, values, selection, selectionArgs)
            SENSES_SINGLE -> rowsUpdated = updateAll(tableName, values, selection, selectionArgs, uri)
            WORD_SINGLE -> rowsUpdated = updateSingle(tableName, values, selection, selectionArgs)
            WORD_ALL -> rowsUpdated = updateAll(tableName, values, selection, selectionArgs, uri)
            else -> throw UnknownUriException(uri)
        }
        context.contentResolver.notifyChange(uri, null)
        return rowsUpdated
    }

    private fun updateSingle(tableName:String, values:ContentValues?, selection:String?, selectionArgs: Array<String>?):Int{
        val sqlDb = mDb!!.writableDatabase
        return sqlDb.update(tableName, values, selection, selectionArgs)
    }

    private fun updateAll(tableName:String, values:ContentValues?, selection:String?, selectionArgs: Array<String>?, uri: Uri) :Int {
        val sqlDb = mDb!!.writableDatabase
        val id = uri.lastPathSegment
        var where = "$STANDARD_ID=$id"
        if(!TextUtils.isEmpty(selection)){
            where += " AND $selection"
        }
        return sqlDb.update(tableName, values, where, selectionArgs)
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val uriType = mUriMatcher.match(uri)

        val rowsDeleted : Int
        val tableName = getTableName(uri)
        var mode = getMode(uri)
        when(mode){
            ALL ->rowsDeleted = deleteAll(tableName, selection, selectionArgs)
            SINGLE -> rowsDeleted = deleteSingle(tableName, selection, selectionArgs, uri)
        }

        context.contentResolver.notifyChange(uri, null)
        return rowsDeleted
    }

    private fun deleteAll(tableName: String, selection: String?, selectionArgs: Array<String>?) : Int{
        val sqlDb = mDb!!.writableDatabase
        return sqlDb.delete(tableName, selection, selectionArgs)
    }

    private fun deleteSingle(tableName: String, selection: String?, selectionArgs: Array<String>?, uri:Uri) :Int{
        val sqlDb = mDb!!.writableDatabase
        val id = uri.lastPathSegment
        var where = "$STANDARD_ID=$id"
        if(!TextUtils.isEmpty(selection)){
            where += " AND $selection"
        }
        return sqlDb.delete(tableName, where, selectionArgs)
    }

    override fun getType(uri: Uri): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val uriType = mUriMatcher.match(uri)
        val sqlDb = mDb!!.writableDatabase
        val tableName = getTableName(uriType)
        val id =sqlDb.insert(tableName, null, values)
        context.contentResolver.notifyChange(uri, null)
        return Uri.parse("$tableName/$id")
    }

    // TODO uzupełnić
    private fun getTableName(uriType:Int):String{
        when(uriType){
            SENSES_ALL->return SENSES_TABLE
            SENSES_SINGLE -> return SENSES_TABLE
            WORD_ALL -> return WORDS_TABLE
            WORD_SINGLE -> return WORDS_TABLE
        }
        return ""
    }

    private fun getTableName(uri:Uri):String{
        val uriType = getUriType(uri)
        when(uriType){
            SENSES_ALL->return SENSES_TABLE
            SENSES_SINGLE -> return SENSES_TABLE
            WORD_ALL -> return WORDS_TABLE
            WORD_SINGLE -> return WORDS_TABLE
            else -> throw UnknownUriException(uri)
        }
        return ""
    }

    private fun getMode(uri:Uri) : Int{
        val uriType = getUriType(uri)
        if(uriType%2 ==1){
            return ALL
        }
        return SINGLE
    }

    private fun getUriType(uri:Uri):Int{
        return mUriMatcher.match(uri)
    }

}