package pl.edu.pwr.plwordnet.models

class Word {

    var id : Long
        get() = id
        set(value) {id = value}

    var word : String
        get() = word
        set(value) {word = value}
}