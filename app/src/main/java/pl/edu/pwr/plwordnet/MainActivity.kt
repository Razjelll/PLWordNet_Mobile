package pl.edu.pwr.plwordnet

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import pl.edu.pwr.plwordnet.database.DatabaseHandler

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
